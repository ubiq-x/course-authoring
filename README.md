# Course Authoring
A Web application (client only) for course authoring.


## Abstract
Much of education has been enhanced by technology.  Some of that enhancement is done by simply digitizing content and making it available on-line while more ambitious attempts endeavor to bring about interactive software to make the concepts taught more accessible to students.

In this repository, I present a Web application designed to aid a course instructor in authoring supplemental educational aids they want to use in their teachings.  This tool is general in that it imposes nothing on neither the educational area nor the kind of materials an instructor would like to use.  Nevertheless, to make things more concrete for the purpose of demonstration, the examples of courses I discuss here are from computer science.

One of the original ways this Web interface was intended to be used was to create content for courses that use [Mastery Grids](https://gitlab.com/ubiq-x/mastery-grids).  Mastery Grids is a user interface and a visualization that allows students to see their progress, see the progress of other students in their class, and to access educational resources.


## Contents
- [Features](#features)
- [Details](#details)
  * [Courses](#courses)
  * [Units, Resources, Providers, and Activities](#units-resources-providers-and-activities)
  * [User Interface](#user-interface)
- [Project Status](#project-status)
- [Dependencies](#dependencies)
- [Data Structures](#data-structures)
- [Demo](#demo)
- [Extending and Integrating](#extending-and-integrating)
- [Acknowledgments](#acknowledgments)
- [Citing](#citing)
- [License](#license)


## Features
- A general way of managing educational resources for any course with only high-level organizational course structure constraints imposed allowing to:
  * Add, edit, and remove a new course
  * Clone an existing course
  * Add, edit, remove, and reorder Resources
  * Select any number of Providers for each Resource
  * Add, edit, remove, and reorder Units
  * Assign Activities to Units
  * View Activity information and filter them
  * Add, edit, remove, and reorder Activities assigned to a Unit
- Ability to view all available course, but also focus on the instructor's courses only
- Fast and simple user interface
- Query string based application state memory


## Details
### Courses
The user interface is divided into three parts.  On the very top is a toolbar that enables coarse course management, e.g., filtering and cloning available courses.  Below that are two table-like areas that allow finer course management, e.g., creating Units.

![My courses](media/course-authoring-01.png)
<p align="center"><b>Figure 1.</b> User interface after the initial load.</p>

### Units, Resources, Providers, and Activities
Very little is imposed on the structure of a course.  More specifically, two organizational constructs are associated with every course: Units and Resources.  _Units_ correspond to thematically isolated ideas taught in the course (e.g., queues, binary trees, or class inheritance) while _Resources_ are educational aids students can access (e.g., questions, examples, readings, lecture notes or videos).  The instructor is free to create the Units to partition the material as they see fit; that division may or may not have a one-to-one correspondence to the course structure as taught in class.

![Courses, Resources, Providers, and Units](media/course-authoring-02.png)
<p align="center"><b>Figure 2.</b> Courses, Resources, Providers, and Units.</p>

Resources would ordinarily be created by someone else because each Resource is associated with a set of _Providers_ or online educational services able to serve content of a given type.  For example, one Provider could contain an organized repository of research articles relevant to the class while another Provider could contain a set of computer program executions visualizations.

![Assigning Activities to Units](media/course-authoring-03.png)
<p align="center"><b>Figure 3.</b> Assigning Activities to Units.</p>

The smallest unit every Resource is broken down into is an _Activity_ which a student can access.  An activity could be a single research paper, a section or chapter from the course textbook, an annotated code example (in the demo called Example), a program execution visualization, a test item that measures the student's understanding (in the demo called Question), or a video.

After having created the Units, the instructor can browse through available Resources selecting one or more Providers from each (Figure 1 and top part of Figure 2).  Then, for each Unit, they can go over each Resource to choose Activities that should be assigned to that Unit (bottom part of Figure 2).  The instructor can filter Activities by the author and by the name and tags.  Furthermore, they can see how the activity looks like in the preview area; if the preview does not prove large enough, they can launch an activity in a new window for closer inspection.  Finally, they can reload an activity which is useful for randomly or procedurally generated content (e.g., a source code with values of variables different every time).

### User Interface
The user interface is color coded.  The instructor's own courses are indicated in blue (Figure 1), the current selection is purple (Figure 2), and all Providers and Activities assigned to Units are shown in green (Figures 1 and 2).

The application remembers its own state using the query string part of the URL making it robust to accidental (or non accidental) reloads and allowing the instructor to create bookmarks to any part of their workflow.


## Project Status
I use this repository to publish source code associated with my original work done in 2014.  While further work might have been carried out as an extension to what I publish here, I see this code as a milestone that marks the end of my involvement in the project and hope it will be useful to someone in its own right.


## Dependencies
None


## Data Structures
Being only a user interface, the present software does not define any persistent data structures.  It does, however, define a data structure it expects from the server-side ([`script/data.js`](script/data.js)).


## Demo
Below is a link to the demo of this Web app.  While the demo uses data much like what a live server would send, it is in fact static data (stored in [`script/data.js`](script/data.js)).  As a result, while all operations (e.g., adding or renaming a unit, removing a resource, assigning an activity to a unit, reordering activities etc.) work as anticipated, the results will not be persisted and the interface will return to its original state upon reload.  Note also that adding courses is not implemented.

Demo: [`http://ubiq-x.gitlab.io/course-authoring`](http://ubiq-x.gitlab.io/course-authoring)

To run the demo on your machine, simply clone this repository into your Web server and navigate your browser to:
```
http://<hostname>:<port>/<path>/course-authoring
```


## Extending and Integrating
There are several obvious ways this Web interface could be extended and integrated into a course management software.  After all, this is very much a prototype.

**Back-end integration.** The Web interface presented here works on static data (see the demo above).  It should be connected to a server-side instance to be able to manage courses persisted in a database.  A sample server response contained in [`script/data.js`](script/data.js) will be helpful in implementing the back-end if the present Web interface was to be used with no alterations.

**User accounts and access rights.** Because only authorized users should be able to modify courses, user accounts should be used.  Furthermore, access rights could be further enforced to ensure that each instructor can see only a select list of all courses available in the system.  For example, an instructor (or an educational institution) may want to keep their courses hidden from other instructors (or institutions).


## Acknowledgments
I worked on this project as a member of the [PAWS lab](http://www.pitt.edu/~paws) at the University of Pittsburgh.


## Citing
Loboda, T.D. (2014).  Course Authoring [Web application].  Available at _https://gitlab.com/ubiq-x/course-authoring_


## License
This project is licensed under the [BSD License](LICENSE.md).
